using UnityEngine;
using UnityEngine.UI;

namespace DefaultNamespace
{
    public class FitInTheHole_Template : MonoBehaviour
    {
        [SerializeField] private Transform[] m_Cubes;
        [SerializeField] private Transform m_PlayerPosition;
        [SerializeField] private Transform[] m_PositionVariants;

        public Transform[] Cubes { get => m_Cubes; }

        private FitInTheHole_FigureTweener tweener;

        private int currentPosition = -1;

        public Transform CurrentTarget { get; private set; }
        public Transform PlayerPosition { get => m_PlayerPosition; }
        
        public Transform[] GetFigure()
        {
            var figure = new Transform[m_Cubes.Length + 1];
            m_Cubes.CopyTo(figure, 0);
            figure[figure.Length - 1] = CurrentTarget;
            return figure;
        }
        
        public void SetupRandomFigure()
        {
            int rand = Random.Range(0, m_PositionVariants.Length);

            for (int i = 0; i < m_PositionVariants.Length; i++)
            {
                if (i == rand)
                {
                    m_PositionVariants[i].gameObject.SetActive(false);
                    CurrentTarget = m_PositionVariants[i].transform;
                    continue;
                }
                
                m_PositionVariants[i].gameObject.SetActive(false);
            }
        }

        private void Update()
        {
            if(tweener != null) return;

            if (Input.GetKeyDown(KeyCode.LeftArrow)) MoveLeft();
            if (Input.GetKeyDown(KeyCode.RightArrow)) MoveRight();
        }

        private void MoveRight()
        {
            if(!IsMovementPossible(-1)) return;

            currentPosition -= 1;
            tweener = m_PlayerPosition.gameObject.AddComponent<FitInTheHole_FigureTweener>();
            tweener.Tween(m_PlayerPosition.position, m_PositionVariants[currentPosition].position, GetFigure());
        }

        private void MoveLeft()
        {
            if(!IsMovementPossible(1)) return;

            currentPosition += 1;
            tweener = m_PlayerPosition.gameObject.AddComponent<FitInTheHole_FigureTweener>();
            tweener.Tween(m_PlayerPosition.position, m_PositionVariants[currentPosition].position, GetFigure());
        }

        private bool IsMovementPossible(int dir)
        {
            return currentPosition + dir >= 0 && currentPosition + dir < m_PositionVariants.Length;
        }
    }
}