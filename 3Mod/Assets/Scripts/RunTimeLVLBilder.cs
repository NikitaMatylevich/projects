﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RunTimeLVLBilder : MonoBehaviour
{
    [SerializeField] private GameObject refWall;
    [SerializeField] private GameObject refTile;
    [SerializeField] private int sizeX = 0;
    [SerializeField] private int sizeZ = 0;

    GameObject[,] tileArr;

    void Start()
    {
        Vector3 tilePosition = this.transform.position;
        float shiftX = refTile.GetComponent<BoxCollider>().bounds.size.x;
        float shiftZ = refTile.GetComponent<BoxCollider>().bounds.size.z;

        tileArr = new GameObject[sizeZ, sizeX];
        for (int i = 0; i < sizeZ; i++)
        {
            for (int j = 0; j < sizeX; j++)
            {
                var tile = Instantiate(refTile, this.transform);
                tile.transform.position = tilePosition;
                tilePosition.x +=  shiftX;
                tileArr[i, j] = tile;
            }
            tilePosition = this.transform.position;
            tilePosition.z -= (shiftZ*(i+1));
        }

        Vector3 wallPosition;
        float wallZSize = refWall.GetComponent<BoxCollider>().bounds.size.z;

        for (int i = 0; i < sizeZ; i++)
        {
            var wall = Instantiate(refWall, this.transform);
            wallPosition = tileArr[i, 0].transform.position;
            wallPosition.x -= (shiftX * 0.5f);
            wall.transform.position = wallPosition;
        }

        for (int i = 0; i < sizeZ; i++)
        {
            var wall = Instantiate(refWall, this.transform);
            wallPosition = tileArr[i, (sizeX-1)].transform.position;
            wallPosition.x += (shiftX * 0.5f);
            wall.transform.position = wallPosition;
        }
    }

  
}
