﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CharacterState
{
    Idle,
    Move,
    Attack,
    Skill,
    Hit,
    Dead,
}

[RequireComponent(typeof(Animator))]

public class PlayerConntroler : MonoBehaviour
{
    private CharacterState characterState;
    [SerializeField] private Animator animator;

    private void Reset()
    {
        animator = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
        characterState = CharacterState.Idle;
        InputController.OnInputAction += OnInputCommand;
    }

    private void OnDestroy()
    {
        InputController.OnInputAction -= OnInputCommand;
    }

    private void OnInputCommand(InputCommand command)
    {
        switch (command)
        {
            case InputCommand.Fire:
                Attack();
                break;
            case InputCommand.Skill:
                Skill();
                break;
        }

        
    }
    private void Attack()
    {
        if (characterState == CharacterState.Attack || characterState == CharacterState.Skill)
        {
            return;
        }
        animator.SetTrigger("Attack");
        DeleyRun.Execute(delegate
        {
            characterState = CharacterState.Idle;
        },
    }

    private void Skill()
    {
        animator.SetTrigger("Skill");
    }
}