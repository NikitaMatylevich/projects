﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class LvlController : MonoBehaviour
{
    [SerializeField] Camera camera;
    [SerializeField] GameObject lvl;

    private int[,] lvlArr;
    LvlData lvlData;
    Vector3 startPlayerPos;

    public GameObject[] PlayerTileArr; 

    int[] prevPlayerPos;
    int[] currentPlayerPos;
    int[] nextPlayerPos;
    int[] z = { 0, -1, 0, 1 };
    int[] x = { 1, 0, -1, 0 };
   

    private void Awake()
    {
        prevPlayerPos = new int[2];
        nextPlayerPos = new int[2];
    }


    
    public int GetNextTileIndex(MoveDirection dir)
    {
        nextPlayerPos[0] = (currentPlayerPos[0] + x[Convert.ToInt32(dir)]);
        nextPlayerPos[1] = (currentPlayerPos[1] + z[Convert.ToInt32(dir)]);

        if (nextPlayerPos[0] < 0 || nextPlayerPos[1] < 0 || nextPlayerPos[0] >= lvlArr.GetLength(0) || nextPlayerPos[1] >= (lvlArr.Length/lvlArr.GetLength(0)) || lvlArr[nextPlayerPos[0],nextPlayerPos[1]] == 0 || lvlArr[nextPlayerPos[0], nextPlayerPos[1]] == 4 || lvlArr[nextPlayerPos[0], nextPlayerPos[1]] == 2)
        {
            return 0;
        }

        prevPlayerPos[0] = currentPlayerPos[0];
        prevPlayerPos[1] = currentPlayerPos[1];
        currentPlayerPos[0] = nextPlayerPos[0];
        currentPlayerPos[1] = nextPlayerPos[1];

        lvlArr[prevPlayerPos[0], prevPlayerPos[1]] = 4;

        return lvlArr[currentPlayerPos[0],currentPlayerPos[1]];
    }

    public void BuildLvl(ObjectPooler objectPooler, int lvlIndex)
    {
        while (lvl.transform.childCount != 0)
        {
            GameObject gameObject = lvl.transform.GetChild(0).gameObject;
            gameObject.transform.parent = null;
            gameObject.SetActive(false);
        }

        var json = Resources.Load<TextAsset>($"Data/Levels/Level{lvlIndex}");
        lvlData = JsonUtility.FromJson<LvlData>(json.ToString());
        
        lvlArr = lvlData.Get2DLvlArr();

        Vector3 pos = Vector3.zero;

        for (int i = 0; i < lvlArr.GetLength(0); i++)
        {
            for (int j = 0; j < (lvlArr.Length / lvlArr.GetLength(0)); j++)
            {
                var obj = lvlArr[i,j];

                switch (obj)
                {
                    case 0:
                        objectPooler.SpawnFromPool("LvlBlock", pos).transform.parent = lvl.transform;
                        pos.x = pos.x - 1; 
                        break;

                    case 1:
                        pos.y = pos.y - 0.25f;
                        objectPooler.SpawnFromPool("LvlPathBlock", pos).transform.parent = lvl.transform;
                        pos.y = pos.y + 0.25f;
                        pos.x = pos.x - 1;
                        break;

                    case 2:
                        currentPlayerPos = new[] { i, j };

                        pos.y = pos.y - 0.25f;
                        objectPooler.SpawnFromPool("LvlStartBlock", pos).transform.parent = lvl.transform;
                        startPlayerPos = pos;
                        pos.y = pos.y + 0.25f;
                        pos.x = pos.x - 1;
                        break;

                    case 3:
                        pos.y = pos.y - 0.25f;
                        objectPooler.SpawnFromPool("LvlFinishBlock", pos).transform.parent = lvl.transform;
                        pos.x = pos.x - 1;
                        pos.y = pos.y + 0.25f;
                        break;

                    default:
                        break;
                }

                
            }

            pos = Vector3.zero;
            pos.z = (pos.z + (i + 1));
        }

        startPlayerPos.y = startPlayerPos.y + 0.25f;
        objectPooler.SpawnFromPool("PlayerTile", startPlayerPos).transform.parent = lvl.transform;
        startPlayerPos.y = startPlayerPos.y + 0.50f;

        PlayerTileArr = new GameObject[lvlData.PlayerSize];

        for (int i = 0; i < lvlData.PlayerSize; i++)
        {
            PlayerTileArr[i] = objectPooler.SpawnFromPool("PlayerTile", startPlayerPos);

            startPlayerPos.y = startPlayerPos.y + 0.5f;
        }

        camera.transform.position = lvlData.CameraPos;
        camera.transform.rotation = lvlData.CameraRotation;

        
    }

    public GameObject[] GetTileArr()
    {
        return PlayerTileArr;
    }

    public bool IsLevelExist(int lvlIndex)
    {
        var json = Resources.Load<TextAsset>($"Data/Levels/Level{lvlIndex}");

        if (json == null) return false;
        else return true;
    }
}

public enum MoveDirection
{
    Forward,
    Right,
    Back,
    Left,
}