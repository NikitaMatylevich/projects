﻿using System.Collections;
using System.Collections.Generic;
using System.Net.Http.Headers;
using UnityEngine;

public class Player : MonoBehaviour
{
    [SerializeField] private GameObject player;
    [SerializeField] private AnimationCurve animationCurve;
    [SerializeField] private AnimationCurve falseMoveAnimationCurve;

    private bool isMoving;
    private GameObject[] playerArr;
    private int currentTile;

    void Start()
    {
        isMoving = false;
        currentTile = 0;
    }


    public void MovePlayer(Vector3 pos, int tileIndex)
    {
        isMoving = true;

        if (tileIndex == 0)
        {
            for (int i = currentTile; i < playerArr.Length; i++)
            {
                StartCoroutine(FalseTileMovment(pos, playerArr[i], (i - currentTile)));
            }

            return;
        }

        for (int i = currentTile; i < playerArr.Length; i++)
        {
            StartCoroutine(MoveTile(pos, playerArr[i], (i - currentTile)));
        }
        
        currentTile++;
    }

    private IEnumerator MoveTile(Vector3 pos, GameObject tile, int index)
    {
        yield return new WaitForSeconds(0.1f * index);

        float time = 0.35f;
        float ofset = 0f;

        Vector3 startTilePos = tile.transform.position;
        pos += startTilePos;

        while (ofset < time)
        {
            ofset += Time.deltaTime;
            tile.transform.position = Vector3.LerpUnclamped(startTilePos, pos, animationCurve.Evaluate(ofset / time));
            yield return null;
        }

        tile.transform.position = pos;

        StartCoroutine(MoveTileDown(new Vector3(0, -0.5f, 0), tile, index));
    }

    private IEnumerator MoveTileDown(Vector3 pos, GameObject tile, int index)
    {
        float time = 0.1f;
        float ofset = 0f;

        Vector3 startTilePos = tile.transform.position;
        pos += tile.transform.position;

        while (ofset < time)
        {
            ofset += Time.deltaTime;
            tile.transform.position = Vector3.Lerp(startTilePos, pos, ofset / time);
            yield return null;
        }

        tile.transform.position = pos;

        if ((index + currentTile) == (playerArr.Length))
        {
            isMoving = false;
        }
    }

    private IEnumerator FalseTileMovment(Vector3 pos, GameObject tile, int index)
    {
        yield return new WaitForSeconds(0.07f * index);

        float time = 0.25f;
        float ofset = 0f;

        Vector3 startTilePos = tile.transform.position;
        pos += startTilePos;

        while (ofset < time)
        {
            ofset += Time.deltaTime;
            tile.transform.position = Vector3.LerpUnclamped(startTilePos, pos, falseMoveAnimationCurve.Evaluate(ofset / time));
            yield return null;
        }

        tile.transform.position = startTilePos;

        if ((index + currentTile) == (playerArr.Length - 1))
        {
            isMoving = false;
        }
    }

    public bool IsLastTile()
    {
        if (currentTile == (playerArr.Length))
        {
            return true;
        }
        else return false;
    }

    public bool IsMoving()
    {
        return isMoving;
    }

    public void SetPlayer(GameObject[] playerTileArr)
    {
        while (player.transform.childCount != 0)
        {
            GameObject gameObject = player.transform.GetChild(0).gameObject;
            gameObject.transform.parent = null;
            gameObject.SetActive(false);
        }

        currentTile = 0;
        isMoving = false;

        playerArr = new GameObject[playerTileArr.Length];

        for (int i = 0; i < playerArr.Length; i++)
        {
            playerTileArr[i].transform.parent = player.transform;
            playerArr[i] = playerTileArr[i];
        }
    }
}
