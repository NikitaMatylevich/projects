﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour
{
    [SerializeField] GameObject winUI;
    [SerializeField] GameObject loseUI;

    public Action OnButtonClick;

    public void TurnWinUIOn()
    {
        winUI.SetActive(true);
    }

    public void TurnWinUIOff()
    {
        winUI.SetActive(false);
    }

    public void TurnLoseUIOn()
    {
        loseUI.SetActive(true);
    }

    public void TurnLoseUIOff()
    {
        loseUI.SetActive(false);
    }

    public void OnButtonClickInvoke()
    { 
        OnButtonClick.Invoke();
    }
      
}
