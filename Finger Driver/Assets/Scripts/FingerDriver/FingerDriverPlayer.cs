using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace FingerDriver
{
    public class FingerDriverPlayer : MonoBehaviour
    {
        [SerializeField] private FingerDriverTrack m_Track;
        [SerializeField] private FingerDriverInput m_Input;
        [SerializeField] private Transform m_trackPoint;
        [SerializeField] private float m_CarSpeed = 2f;
        [SerializeField] private float m_MaxSteer = 90f;
        [SerializeField] private GameObject m_Text;

        private float time = 0f;
        private int score = 0;

        private void Update()
        {
            if (m_Track.IsPointInTrack(m_trackPoint.position))
            {
                transform.Translate(transform.up * Time.deltaTime * m_CarSpeed, Space.World);
                transform.Rotate(0f, 0f, m_MaxSteer * m_Input.SteerAxis * Time.deltaTime);

                time += Time.deltaTime;
                if (time >= 1.5)
                {
                    time = 0f;
                    score++;
                    m_Text.GetComponent<Text>().text = $"Score:{score}"; 
                }
            }
            else
            {
              
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }
    }
}