﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HopPalyer : MonoBehaviour
{

    [SerializeField] private AnimationCurve m_JumpCurve;
    [SerializeField] private float m_JampHeight = 1f;
    [SerializeField] private float m_JumpDistance = 2f;
    [SerializeField] private float m_BallSpeed = 1f;
    [SerializeField] private HopInput m_Input;
    [SerializeField] private HopTrack m_Track;

    private float iteration;
    private float startZ;

    // Update is called once per frame
    void Update()
    {
        var pos = transform.position;
        pos.x = Mathf.Lerp(pos.x, m_Input.Strafe, Time.deltaTime * 5f);
        pos.y = m_JumpCurve.Evaluate(iteration) * m_JampHeight;

        pos.z = startZ + iteration * m_JumpDistance;
        transform.position = pos;

        iteration += Time.deltaTime * m_BallSpeed;

        if (iteration < 1f)
        {
            return;
        }

        iteration = 0;
        startZ += m_JumpDistance;

        switch (m_Track.IsBallOnPlatform(transform.position))
        {
            case 0:
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
                break;
            case 1:
                StatsReset();
                break;
            case 2:
                StatsReset();
                AddJump();
                break;
            case 3:
                StatsReset();
                AddBoost();
                break;
            default:
                break;
        }
         
    }

   public void StatsReset()
    {
        m_JampHeight = 1f;
        m_JumpDistance = 2f;
        m_BallSpeed = 1f;
    }

    public void AddBoost()
    {
        StatsReset();
        m_BallSpeed = 2f;
        
    }

    public void AddJump()
    {
        StatsReset();
        m_JumpDistance = 4f;
    }
}
