﻿using System.Collections;
using System.Collections.Generic;
using UnityEditorInternal;
using UnityEngine;

public class HopTrack : MonoBehaviour
{
    [SerializeField] private HopPalyer m_Player;
    [SerializeField] private GameObject m_RedPlatform;
    [SerializeField] private GameObject[] m_Platforms;
    private List<GameObject> platforms = new List<GameObject>();


    void Start()
    {
        int rand;

        platforms.Add(m_Platforms[0]);

        for (int i = 0; i < 25; i++)
        {
            rand = Random.Range(0, 3);
            GameObject obj = Instantiate(m_Platforms[rand], transform);
            Vector3 pos = Vector3.zero;

            pos.z = 2 * (i + 1);
            pos.x = Random.Range(-1, 2);
            obj.transform.position = pos;

            if (rand == 1)
            {
                obj.name = $"BoostPlatform {i}";
            }
            else if (rand == 2)
            {
                obj.name = $"HeightPlatform {i}";
            }
            else
            {
                obj.name = $"Platform {i}";
            }
            
            platforms.Add(obj);
        }
    }

    public int IsBallOnPlatform(Vector3 position)
    {
        position.y = 0f;

        var nereastPlatform = platforms[0];

        for (int i = 1; i < platforms.Count; i++)
        {
            var platformZ = platforms[i].transform.position.z;
            if (platformZ + 0.5f < position.z)
            {
                continue;
            }

            if (platformZ - position.z > 1f)
            {
                continue;
            }

            nereastPlatform = platforms[i];
            platforms[i - 1].SetActive(true);

            break;
        }

        

        float minX = nereastPlatform.transform.position.x - 0.5f;
        float maxX = nereastPlatform.transform.position.x + 0.5f;

        if (position.x > minX && position.x < maxX)
        {
            m_RedPlatform.transform.position = nereastPlatform.transform.position;
            nereastPlatform.SetActive(false);
            if (nereastPlatform.name.Contains("BoostPlatform"))
            {
               
                return 3;
                
            }
            else if (nereastPlatform.name.Contains("HeightPlatform"))
            {
               
                return 2;
            }
            else
            {
              
                return 1;
            }
        }

        return 0;
    }
}
