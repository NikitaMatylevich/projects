﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class LadingScript : MonoBehaviour
{
    [SerializeField] private Text Text;
    [SerializeField] private Text HintText;

    private string[] hints = new string[3] {"Жизнь надо жить!", "Если дверь в туадет закрыта - там кто то есть!", "Лучше лучше чем хуже!"};


    private float hintTimer;
    private float time = 0;
    int count;
    int rand;

    
    void Start()
    {
        rand = Random.Range(0, 3);
        HintText.GetComponent<Text>().text = hints[rand];
        Text.GetComponent<Text>().text = "Loadind";
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;
        hintTimer += time; 
        if (time >= 0.2)
        {
            ChangeText();
            time = 0;
        }
        if (hintTimer >= 100)
        {
            HintTextChange();
            hintTimer = 0;
        }
    }

    int ChangeText()
    {
        if (count == 3)
        {
            Text.GetComponent<Text>().text = "Loading";
            count = 0;
            return 0;
        }
        Text.GetComponent<Text>().text +=".";
        count++;
        return 0;
    }

    void HintTextChange()
    {
        rand = Random.Range(0, 3);
        HintText.GetComponent<Text>().text = hints[rand];
    }
}
