﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.Remoting.Metadata.W3cXsd2001;
using UnityEditor;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(BoxCollider2D))]
public class StaticObject : MonoBehaviour, IHitBox
{
	[SerializeField] private LevlObjectData objectData;
	[SerializeField] private GameObject coin;
	 private int health = 1;
	private Rigidbody2D rigidbody;

	private void Start()
	{
		health = objectData.Helth;
		rigidbody = GetComponent<Rigidbody2D>();
		rigidbody.bodyType = objectData.isStatic ? RigidbodyType2D.Static : RigidbodyType2D.Dynamic;
	}


#if UNITY_EDITOR
	[ContextMenu("Rename this")]
	private void Rename()
	{
		if (objectData != null)
		{
			gameObject.name = objectData.Name;
		}
	}
	[ContextMenu("Move Right")]
	private void MoveRight()
	{
		Move(Vector2.right);
	}
	[ContextMenu("Move Left")]
	private void MoveLeft()
	{
		Move(Vector2.left);
	}
	[ContextMenu("Move")]
	private void Move(Vector2 direction)
	{
		var collider = GetComponent<Collider2D>();
		var size = collider.bounds.size;

		transform.Translate(direction * size);
	}

#endif
  
	public int Health
	{
		get => health;
		private set
		{
			health = value;
			if (health <= 0) Die();
		}
	}

	public void Die()
	{
		CoinThrow();
		Destroy(gameObject);

	}

	public void Hit(int damage)
	{
		 Health -= damage;
	}

	private void CoinThrow()
	{
		int rand;
		
		for (int i = 0; i < 3; i++)
		{
			rand = Random.Range(-1, 2);
			var	obj = Instantiate(coin);
			obj.transform.position = this.transform.position;
			obj.AddComponent<BoxCollider2D>();
			obj.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Dynamic;
			obj.GetComponent<Rigidbody2D>().AddForce(new Vector2(rand, 10), ForceMode2D.Impulse);
		}
		
		 
	}


}
