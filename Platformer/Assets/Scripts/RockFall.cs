﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RockFall : MonoBehaviour
{
    [SerializeField] private GameObject rock;
    [SerializeField] private bool falling;


    private void Start()
    {
        if (falling)
        {
            StartCoroutine(Fall());
        }
        
    }

    private IEnumerator Fall()
    {
        while (true)
        {
            int shift = Random.Range(-5, 6);

            Vector2 pos = transform.position;
            pos.x += shift;

            GameObject rocks = Instantiate(rock, this.transform);
            rocks.transform.position = pos;
            yield return new WaitForSeconds(1f);
        }

       


    }
}
