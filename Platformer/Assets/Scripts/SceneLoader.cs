﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    private static string nextLevel;
    public static void LoadLevel(string level)
    {
        nextLevel = level;
        SceneManager.LoadScene(0);
    }

    private IEnumerator Start()
    {
        GameManager.SetGameState(GameState.Loading);

        yield return new WaitForSeconds(6f);

        if(string.IsNullOrEmpty(nextLevel))
        {
            SceneManager.LoadScene(1);
            yield break;
        }

        AsyncOperation loading = null;

        loading = SceneManager.LoadSceneAsync(nextLevel, LoadSceneMode.Additive);

        while(!loading.isDone)
            yield return null;

        nextLevel = null;
        SceneManager.UnloadSceneAsync("LoadingScene");
    }
}
