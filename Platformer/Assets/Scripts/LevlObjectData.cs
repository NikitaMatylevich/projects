﻿using UnityEngine;

[CreateAssetMenu(fileName = "Data", menuName = "Objects/LevelObject", order = 1 )]
public class LevlObjectData : ScriptableObject
{
    public string Name = "New level object Name";
    public bool isStatic;
    public int Helth = 1;
}
