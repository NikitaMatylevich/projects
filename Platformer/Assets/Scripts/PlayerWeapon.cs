﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerWeapon : MonoBehaviour, IDamage
{
    [SerializeField] private WeponData weponData;
    [SerializeField] private Transform shootPoint;

    [SerializeField] private string axis = "Fire1";

    public string Axis => axis;
    public int Damage => weponData.weponDamage;

    public void SetDamage()
    {
        var target = GetTargert();

        target?.Hit(Damage);
    }

    private IHitBox GetTargert()
    {
        IHitBox target = null;

        RaycastHit2D hit = Physics2D.Raycast(shootPoint.position, shootPoint.right, weponData.reange);

        if (hit.collider != null)
        {
            target = hit.transform.root.GetComponent<IHitBox>();
        }

        return target;
    }
}
