﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class PlatformGenerator : MonoBehaviour
{
    [SerializeField] private int size;
    [SerializeField] private GameObject startPlatform;
    [SerializeField] private GameObject midlePaltform;
    [SerializeField] private GameObject endPlatform;
    static private int count = 1; 

    [ContextMenu("Generate Platform")]
    private void Generate()
    {
        var plaform = new GameObject();
        plaform.name = $"Platform{count}";

        var pos = Vector2.zero;

        plaform.transform.position = pos;

        var start = Instantiate(startPlatform, plaform.transform);
        start.transform.position = pos;
        float shift = start.gameObject.GetComponent<Collider2D>().bounds.size.x;
        pos.x = pos.x + shift;

        for (int i = 0; i < size; i++)
        {
            var midle = Instantiate(midlePaltform, plaform.transform);
            midle.transform.position = pos;
            shift = midle.gameObject.GetComponent<Collider2D>().bounds.size.x;
            pos.x = pos.x + shift;

        }

        var end = Instantiate(endPlatform, plaform.transform);
        end.transform.position = pos;

        count++;
    }

}
