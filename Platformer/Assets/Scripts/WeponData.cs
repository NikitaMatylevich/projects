﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "WeponData", menuName = "Objects/Wepon object", order = 0)]

public class WeponData : ScriptableObject
{
    public string weponName = "Wepon Name";
    public int weponDamage = 1;
    public float reange = 1f;
    public float fireRate = 1f;
    public GameObject bullet;
    public float bulletSpeed;
}
