﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMove : MonoBehaviour
{
    [SerializeField] private GameObject Platform;
    [SerializeField] private float speed;
    [SerializeField] private bool Horizontal;
    [SerializeField] private float distance = 6f;
    [SerializeField] private bool Vertical;
    private Vector3 startPosition;
    private Vector3 targetPosition;


    private void Start()
    {
        if (Horizontal)
        {
            startPosition = transform.position;
            targetPosition = transform.position;
            targetPosition.x += distance;

            StartCoroutine(MovmentProcess());

        }
        if (Vertical)
        {
            startPosition = transform.position;
            targetPosition = transform.position;
            targetPosition.y += distance;

            StartCoroutine(MovmentProcess());
        }
    }
   

    private IEnumerator MovmentProcess()
    {
        var k = 0f;
        var dir = 1f;

        while (true)
        {
            transform.position = Vector3.Lerp(startPosition, targetPosition, k);
            k += Time.deltaTime * dir * speed;

            if (k>1f)
            {
                dir = -1;
                k = 1;
                yield return new WaitForSeconds(1f);
            }

            if (k < 0)
            {
                dir = 1;
                k = 0;
                yield return new WaitForSeconds(1f);
            }

            yield return null;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        bool IsMovmentObject = other.transform.GetComponent<PlayerMovement>();
        if (IsMovmentObject)
        {
            other.transform.parent = transform;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.parent == transform)
        {
            other.transform.parent = null;
        }
    }
}
