﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEditor.VersionControl;
using UnityEngine;

public class GameDataEditor : EditorWindow
{
    public GameData GameData;
    private string gameDataFilePass = "/Data/GameData.json";

    [MenuItem("Tools/Game Data Editor")]
    private static void Init()
    {
        EditorWindow.GetWindow(typeof(GameDataEditor)).Show();
    }

    private void OnGUI()
    {
        if (GameData != null)
        {
            SerializedObject serializedObject = new SerializedObject(this);
            SerializedProperty serializedProperty = serializedObject.FindProperty("GameData");
            EditorGUILayout.PropertyField(serializedProperty, true);
            serializedObject.ApplyModifiedProperties();

            if (GUILayout.Button("Save Data"))
            {
                SaveGameData();
            }
        }

        if (GUILayout.Button("Load Data"))
        {
            LoadGameData();
        }
    }

    private void LoadGameData()
    {
        var path = Application.dataPath + gameDataFilePass;
        if (File.Exists(path))
        {
            var dataAsJason = File.ReadAllText(path);
            GameData = JsonUtility.FromJson<GameData>(dataAsJason);
        }
        else
        {
            GameData = new GameData();
        }
    }
    
    private void SaveGameData()
    {
        var dataAsJson = JsonUtility.ToJson(GameData);
        var path = Application.dataPath + gameDataFilePass;
        File.WriteAllText(path, dataAsJson);
        AssetDatabase.Refresh();
    }
}
