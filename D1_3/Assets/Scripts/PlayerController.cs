﻿using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Xml.Serialization;
using UnityEngine;

public enum ChatacterState
{
	Idle,
	Move,
	Attack,
	Skill,
	Hit,
	Dead,

}

[RequireComponent(typeof(Animator))]
public class PlayerController : MonoBehaviour
{
	private ChatacterState characterState;
	[SerializeField] private Animator animator;
	[SerializeField] private GameObject ballPrefab;
	[SerializeField] private Transform firePoint;
	[SerializeField] private Transform skillPoint;

	public void AttackEvent()
	{
		var obj = ObjectsPool.Instance.GetObject(ballPrefab);
		obj.transform.position = firePoint.transform.position;
		obj.transform.rotation = firePoint.transform.rotation;
		
		var rig = obj.GetComponent<Rigidbody>();
		if (rig != null)
		{
			rig.velocity = Vector3.zero;
			rig.AddForce(Vector3.forward * 5f, ForceMode.Impulse);
		}
	}

	private void Reset()
	{
		animator = GetComponent<Animator>();
	}

	// Start is called before the first frame update
	void Start()
	{
		characterState = ChatacterState.Idle;
		InputController.OnInputAction += OnInputCommand;
		
		ObjectsPool.Instance.PrepareObjcets(ballPrefab, 5);
		ObjectsPool.Instance.PrepareObjcets(ballPrefab, 10);
	}

	private void OnDestroy()
	{
		InputController.OnInputAction -= OnInputCommand;
	}

	private void OnInputCommand(InputCommand command)
	{
		switch (command)
		{
			case InputCommand.Fire:
				Attack();
				break;
			case InputCommand.Skill:
				Skill();
				break;
			case InputCommand.Deadshot:
				DeadShot();
				break;
			default:
				break;
		}
	}

	private void DeadShot()
	{
		if (characterState == ChatacterState.Attack || characterState == ChatacterState.Skill)
			return;
		
		animator.SetTrigger("SkillTrigger");
		DelayRun.Execute(delegate { characterState = ChatacterState.Idle; }, 1.65f, gameObject);
		DelayRun.Execute(DeadShotExecute, 1.35f, gameObject);
	}

	private void DeadShotExecute()
	{
		var hit = new RaycastHit();
		if(!Physics.Raycast(skillPoint.position, skillPoint.forward, out hit)) 
			return;

		var health = hit.transform.GetComponent<Health>();
		if(health != null)
			health.SetDamage(int.MaxValue);
	}
	
	private void Attack()
	{
		if (characterState == ChatacterState.Attack || characterState == ChatacterState.Skill)
			return;

		animator.SetTrigger("attack");
		characterState = ChatacterState.Attack;

		DelayRun.Execute(delegate
		{
			characterState = ChatacterState.Idle;
		}, 0.5f, gameObject);
	}

	private void Skill()
	{
		if (characterState == ChatacterState.Attack || characterState == ChatacterState.Skill)
			return;

		animator.SetTrigger("skill");
		characterState = ChatacterState.Skill;

		DelayRun.Execute(delegate
		{
			characterState = ChatacterState.Idle;
		}, 1f, gameObject);
	}
}
