﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HeadTracking : MonoBehaviour
{
    public enum Target
    {
        Player,
        Ball,
        
    }

    [SerializeField] private Target target;
    [SerializeField] private Vector3 angleFix = new Vector3(180f, 0f, 90f);

    private Transform targetTransform;
   

    // Start is called before the first frame update
    

    private void Update()
    {
        var containerObject = FindObjectOfType<BonusContainer>();
        var ballObject = FindObjectOfType<Ball>();
        var player = FindObjectOfType<PlayerController>();

        if (ballObject != null)
        {
            targetTransform = ballObject.transform;
        }
        else if (containerObject != null)
        {
            targetTransform = containerObject.transform;

        }
        else
        {
            targetTransform = player.transform;
        }
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Quaternion quaternion = Quaternion.LookRotation(targetTransform.position - transform.position);

        if (quaternion.eulerAngles.y < 90f) quaternion.eulerAngles = new Vector3(quaternion.eulerAngles.x, 90f, quaternion.eulerAngles.z);
        if (quaternion.eulerAngles.y > 270f) quaternion.eulerAngles = new Vector3(quaternion.eulerAngles.x, 270f, quaternion.eulerAngles.z);

        transform.rotation = quaternion;
        transform.Rotate(angleFix);
    }
}
