﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using UnityEditor;
using UnityEngine;

public static class Profile
{
    [System.Serializable]
    private class MainData
    {
        public List<int> LevelStars;
        public int Money = 10;
    }
    
    [System.Serializable]
    private class PlayerData
    {
        public bool Sound = true;
        public bool Music = true;
    }

    private static MainData mainData;
    private static PlayerData playerData;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void CheckMainData()
    {
        Debug.Log("Init profile main data");
        
        if(mainData != null) return;
        
        if (!PlayerPrefs.HasKey("MainData"))
        {
            mainData = new MainData();
            PlayerPrefs.SetString("MainData", JsonUtility.ToJson(mainData));
        }

        mainData = JsonUtility.FromJson<MainData>(PlayerPrefs.GetString("MainData"));
    }
    
    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
    private static void CheckPlayerData()
    {
        Debug.Log("Init profile main data");
        
        if(playerData != null) return;
        
        if (!PlayerPrefs.HasKey("PlayerData"))
        {
            playerData = new PlayerData();
            PlayerPrefs.SetString("PlayerData", JsonUtility.ToJson(playerData));
        }

        playerData = JsonUtility.FromJson<PlayerData>(PlayerPrefs.GetString("PlayerData"));
    }

    public static void Save(bool main = true, bool player = true)
    {
        if (main) PlayerPrefs.SetString("MainData", JsonUtility.ToJson(mainData));
        if (player) PlayerPrefs.SetString("PlayerData", JsonUtility.ToJson(playerData));
    }

    public static int Money
    {
        get => mainData.Money;
        set
        {
            mainData.Money = value;
            if (mainData.Money < 0) mainData.Money = 0;
            
            Save(player:false);
        }
    }

    public static int GetOpenedLevelCount => mainData.LevelStars.Count;

    public static int GetLenelStars(int level)
    {
        if (level >= mainData.LevelStars.Count) return -1;
        return mainData.LevelStars[level];
    }

    public static void SetLevelStars(int level, int stars)
    {
        if (level > mainData.LevelStars.Count)
        {
            Debug.Log("Level is not opened");
            return;
        }

        if (level == mainData.LevelStars.Count)
        {
            stars = Mathf.Clamp(stars, 0, 3);
            mainData.LevelStars.Add(stars);
            Save(player:false);
        }

        if (stars > mainData.LevelStars[level])
        {
            mainData.LevelStars[level] = stars;
            Save(player: false);
        }
    }
}
